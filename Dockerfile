FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
RUN apt-get update \
    && apt-get install -y --allow-unauthenticated \
        libc6-dev \
        libgdiplus \
        libx11-dev \
     && rm -rf /var/lib/apt/lists/*  
ARG source
WORKDIR /app
EXPOSE 80
COPY ${source:-FileProcessing.Api/obj/Docker/publish} .
ENTRYPOINT ["dotnet", "FileProcessing.Api.dll"]