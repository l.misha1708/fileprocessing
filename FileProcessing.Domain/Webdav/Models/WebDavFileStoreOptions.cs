namespace FileProcessing.Domain.Webdav.Models
{
    /// <summary>
    /// Настройки фалового хранилища
    /// </summary>
    public class WebDavFileStoreOptions
    {
        /// <summary>
        /// Название корневого контейнера
        /// </summary>
        public string Container { get; set; }
        /// <summary>
        /// URI сервера WebDav
        /// </summary>
        public string ServerUri { get; set; }
        /// <summary>
        /// Пользователь (для авторизации)
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Пароль (для авторизации)
        /// </summary>
        public string Password { get; set; }
    }
}