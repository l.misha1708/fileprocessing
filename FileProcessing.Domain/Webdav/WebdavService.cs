using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FileProcessing.Domain.Webdav.Models;

namespace FileProcessing.Domain.Webdav
{
    /// <inheritdoc/>
    public class WebdavService : IWebdavService
    {
        private readonly HttpClient _client;

        /// <summary/>
        public WebdavService(WebDavFileStoreOptions options)
        {
            var uri = options.ServerUri;
            if (!string.IsNullOrWhiteSpace(options.Container))
            {
                uri += "/" + options.Container;
            }

            _client = new HttpClient
            {
                BaseAddress = new Uri(uri)
            };
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes($"{options.Login}:{options.Password}")));
        }

        /// <inheritdoc/>
       public async Task UploadFile(Stream content, string fileName, CancellationTokenSource cancellationToken = null)
        {
            var path = GetPath(fileName);
            var response = await _client.PutAsync(path, new StreamContent(content),
                cancellationToken?.Token ?? CancellationToken.None);
            if (response.StatusCode != HttpStatusCode.Created)
                throw new Exception($"Ошибка записи файла в хранилище: {response.StatusCode} {response.ReasonPhrase}");
        }

        
        /// <inheritdoc/>
        public async Task<Stream> DownloadFile(string fileName, CancellationTokenSource cancellationToken = null)
        {
            var path = GetPath(fileName);
            var response = await _client.GetAsync(path, cancellationToken?.Token ?? CancellationToken.None);
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException($"Ошибка при получении файла из хранилища. {response.StatusCode} {response.ReasonPhrase}");
            }
            return await response.Content.ReadAsStreamAsync();
        }
        
        /// <summary>
        /// Создание папки для размещения файла
        /// </summary>
        /// <param name="fileName">имя файла</param>
        /// <returns></returns>
        private static string GetPath(string fileName)
        {
            var result = $"{fileName.Substring(0, 2)}/{fileName.Substring(2, 2)}/{fileName}";
            return result;
        }
    }
}