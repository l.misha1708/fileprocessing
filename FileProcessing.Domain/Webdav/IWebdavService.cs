using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace FileProcessing.Domain.Webdav
{
    /// <summary>
    /// Сервис по работе с Webdav
    /// </summary>
    public interface IWebdavService
    {
        /// <summary>
        /// Загрузка файла в хранилище
        /// </summary>
        /// <param name="fileContent">Файл</param>
        /// <param name="fileName">Имя файла</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task UploadFile(Stream content, string fileName, CancellationTokenSource cancellationToken = null);

        /// <summary>
        /// Получение файла из хранилища
        /// </summary>
        /// <param name="fullFilePath">Полный путь к файлу</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Stream> DownloadFile(string fullFilePath, CancellationTokenSource cancellationToken = null);

    }
}