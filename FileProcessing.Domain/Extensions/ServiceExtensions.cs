using System;
using FileProcessing.Domain.Files;
using FileProcessing.Domain.Webdav;
using FileProcessing.Domain.Webdav.Models;
using Microsoft.Extensions.DependencyInjection;

namespace FileProcessing.Domain.Extensions
{
    /// <summary>
    /// Расширения сервисов
    /// </summary>
    public static class ServiceExtensions
    {
        /// <summary>
        /// Подключение сервисов
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IFileService, FileService>();
            return services;
        }
        
        /// <summary>
        /// Подключение Webdav
        /// </summary>
        /// <param name="services"></param>
        /// <param name="setup"></param>
        /// <returns></returns>
        public static IServiceCollection AddWebDavFileRepository(this IServiceCollection services, Action<WebDavFileStoreOptions> setup)
        {
            var options = new WebDavFileStoreOptions();
            setup?.Invoke(options);
            services.AddSingleton(options);

            services.AddScoped<IWebdavService, WebdavService>();

            return services;
        }
    }
}