using System.IO;

namespace FileProcessing.Domain.Extensions
{
    public class FileExtensions
    {
        /// <summary>
        /// Перевод массива байт в Stream
        /// </summary>
        /// <param name="byteArray">Массив байт</param>
        /// <returns></returns>
        public static MemoryStream ByteArrayToStream(byte[] byteArray)
        {
            var ms = new MemoryStream();
            var writer = new BinaryWriter(ms);
            writer.Write(byteArray);
            writer.Flush();
            ms.Position = 0;
            return ms;
        }
    }
}