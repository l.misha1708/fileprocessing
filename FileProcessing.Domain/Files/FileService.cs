using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FileProcessing.Domain.Files
{
    /// <inheritdoc/>
    public class FileService : IFileService
    {
        /// <inheritdoc/>
        public async Task<Stream> UploadFile(IFormFile file, CancellationToken token)
        {
            var ms = new MemoryStream();
            await file.CopyToAsync(ms, token);
            return ms;
        }

        /// <inheritdoc/>
        public async Task<Stream> UploadFileByUrl(string url, CancellationToken token)
        {
            using (var client = new HttpClient())
            {
                var result = await client.GetAsync(url, token);
                if (result.IsSuccessStatusCode)
                {
                    Stream stream = await result.Content.ReadAsStreamAsync();
                    return stream;
                }

                return null;
            }
        }

        /// <inheritdoc/>
        public byte[] CreateThumbnail(Stream stream)
        {
            Image image = Image.FromStream(stream);
            Image thumbImage = image.GetThumbnailImage(100, 100, () => true, IntPtr.Zero);

            MemoryStream ms = new MemoryStream();
            thumbImage.Save(ms, ImageFormat.Jpeg);
            return ms.ToArray();
        }
    }
}