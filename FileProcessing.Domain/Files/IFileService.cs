using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FileProcessing.Domain.Files
{
    /// <summary>
    /// Сервис по работе с файлами
    /// </summary>
    public interface IFileService
    {
        /// <summary>
        /// Загрузка файла напрямую
        /// </summary>
        /// <param name="file">Файл</param>
        /// <returns></returns>
        Task<Stream> UploadFile(IFormFile file, CancellationToken token);

        /// <summary>
        /// Загрузка файла по ссылке
        /// </summary>
        /// <param name="url">ссылка на файл</param>
        /// <returns></returns>
        Task<Stream> UploadFileByUrl(string url, CancellationToken token);

        /// <summary>
        /// Создание превью изображения
        /// </summary>
        /// <param name="stream">Поток с изображением</param>
        /// <returns></returns>
        byte[] CreateThumbnail(Stream stream);
    }
}