using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using FileProcessing.Domain.Files;
using FileProcessing.Domain.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using NUnit.Framework;

namespace Tests.UnitTests
{
    /// <summary>
    /// Unit тесты
    /// </summary>
    [TestFixture]
    public class FileServiceTests
    {
        private IFileService _fileService;

        /// <summary/>
        [SetUp]
        public void Init()
        {
            _fileService = new FileService();
        }

        /// <summary>
        /// Тест создания превью фото
        /// </summary>
        [Test]
        public void CreateThumbnail()
        {
            //Arrange
            var imageContent = File.ReadAllBytes(
                "../../../TestFiles/testImage1.jpg");

            var ms = FileExtensions.ByteArrayToStream(imageContent);

            //Act
            var result = _fileService.CreateThumbnail(ms);

            var msResult = FileExtensions.ByteArrayToStream(result);
            
            var image = Image.FromStream(msResult);
            
            //Assert
            Assert.AreEqual(image.Height, 100);
            Assert.AreEqual(image.Width, 100);
            Assert.IsNotNull(image);
        }


        /// <summary>
        /// Тест загрузки файла напрямую
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task LoadFileDirectly()
        {
            //Arrange
            var imageContent = await File.ReadAllBytesAsync(
                "../../../TestFiles/testImage1.jpg");

            //Setup IFormFile
            var ms = FileExtensions.ByteArrayToStream(imageContent);
            IFormFile file = new FormFile(ms, 0, ms.Length, "testImage", "testImage.jpg");

            //Act
            var result = await _fileService.UploadFile(file, CancellationToken.None);

            //Assert
            Assert.IsInstanceOf(typeof(Stream), result);
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Тест загрузки файла по ссылке
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task LoadFileByUrl()
        {
            //Arrange
            var url = "http://easyjava.ru/wp-content/uploads/2015/08/unittestall-300x225.jpg";

            //Act
            var result = await _fileService.UploadFileByUrl(url, CancellationToken.None);

            //Assert
            Assert.IsInstanceOf(typeof(Stream), result);
            //Assert.IsNotEmpty(result);
            Assert.IsNotNull(result);
        }
    }
}
