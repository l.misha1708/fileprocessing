using NUnit.Framework;
using Microsoft.Extensions.DependencyInjection;
using System;
using FileProcessing.Domain.Extensions;

namespace Tests
{
    /// <summary>
    /// Класс инициализации Unit тестов
    /// </summary>
    [SetUpFixture]
    public class TestInitializer
    {
        public static IServiceProvider _Provider { get; private set; }

        /// <summary>
        /// Инициализация
        /// </summary>
        [OneTimeSetUp]
        public void Initializer()
        {
            var services = new ServiceCollection();

            services.AddServices();

            _Provider = services.BuildServiceProvider();
        }

        [OneTimeTearDown]
        public void CleanUp()
        {
                        
        }
    }
}