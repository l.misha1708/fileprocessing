using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FileProcessing.Domain.Files;
using FileProcessing.Domain.Webdav;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace FileProcessing.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы с файлами
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : Controller
    {
        private readonly IFileService _fileService;
        private readonly IWebdavService _webdavService;
        private readonly IApplicationLifetime _applicationLifetime;
        
        public FilesController(IFileService fileService, IApplicationLifetime applicationLifetime, IWebdavService webdavService)
        {
            _fileService = fileService;
            _applicationLifetime = applicationLifetime;
            _webdavService = webdavService;
        }

        /// <summary>
        /// Загрузка изображения
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<List<FileContentResult>> UploadFile([FromForm]List<string> urls)
        {
            CancellationToken token = _applicationLifetime.ApplicationStopping;
            var files = new List<FileContentResult>();
            
            //Load files directly and processing
            if (Request.Form.Files.Count != 0)
            {
                foreach (var file in Request.Form.Files)
                {
                    var stream = await _fileService.UploadFile(file,token);
                    await _webdavService.UploadFile(stream, Guid.NewGuid().ToString(), CancellationTokenSource.CreateLinkedTokenSource(token));
                    files.Add(File(_fileService.CreateThumbnail(stream), "image/jpeg"));
                }
            }
            //Load files by url and processing 
            if (urls.Count != 0)
            {
                foreach (var url in urls)
                {
                    var stream = await _fileService.UploadFileByUrl(url,token);
                    await _webdavService.UploadFile(stream, Guid.NewGuid().ToString(), CancellationTokenSource.CreateLinkedTokenSource(token));
                    files.Add(File(_fileService.CreateThumbnail(stream), "image/jpeg"));  
                }
            }
            
            else
            {
                return null;
            }
            return files;
        }
        
        
    }
}