﻿using System;
using FileProcessing.Domain.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace FileProcessing.Api
{
    /// <summary/>
    public class Startup
    {
        /// <summary/>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary/>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            
            services.AddServices();
            
            services.AddWebDavFileRepository(options =>
            {
                options.Container = Configuration["WebDavFileStore:Container"];
                options.ServerUri = Configuration["WebDavFileStore:ServerUri"];
                options.Login = Configuration["WebDavFileStore:Login"];
                options.Password = Configuration["WebDavFileStore:Password"];
            });
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "File processing API", Version = "v1" });
            });
        }

        /// <summary/>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime applicationLifetime)
        {
            applicationLifetime.ApplicationStopping.Register(OnShutdown);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
                app.UseSwagger();

                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                    c.RoutePrefix = "api/help";
                });
            }
            else
            {
                app.UseHsts();
            }

            app.UseMvc();
        }

        /// <summary>
        /// При выключении
        /// </summary>
        private void OnShutdown()
        {
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("Finished.");
        }
    }
}